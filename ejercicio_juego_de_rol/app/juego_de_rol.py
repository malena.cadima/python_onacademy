# pylint:disable = R0903
# pylint:disable = W0212

class Posicion:
    def __init__(self, posicion_x):
        self.__posicion_x = posicion_x

    def __eq__(self, otra_posicion):
        return self.obtener_posicion_x() == otra_posicion.obtener_posicion_x()

    def sumar(self, cantidad):
        return self.obtener_posicion_x() + cantidad

    def obtener_posicion_x(self):
        return self.__posicion_x

class Adelante():
    def __init__(self, posicion_i):
        self.__posicion_i = posicion_i.obtener_posicion_x()

    def mover(self, cantidad):
        nueva_posicion = Posicion(self.__posicion_i + cantidad)
        return nueva_posicion

class Atras():
    def __init__(self, posicion_i):
        self.__posicion_i = posicion_i.obtener_posicion_x()

    def mover(self, cantidad):
        nueva_posicion = Posicion(self.__posicion_i - cantidad)
        return nueva_posicion

class Personaje:
    def __init__(self, vida, posicion, velocidad):
        self.__vida = vida
        self.__posicion = posicion
        self.__velocidad = velocidad

    def recibir_ataque(self, ataque_recibido):
        self.__vida -= ataque_recibido
        if self.__vida <= 0:
            raise ValueError('ta muerto')

    def vida(self):
        return self.__vida

    def posicion(self):
        return self.__posicion

    def mover(self, direccion):
        self.__posicion = direccion.mover(self.__velocidad)


class Campesino(Personaje):
    def __init__(self, vida, posicion, velocidad, cosecha):
        super().__init__(vida, posicion, velocidad)
        self.__cosecha = cosecha

    def cosechar(self):
        cantidad_cosechada = 1
        self.__cosecha += cantidad_cosechada
        return cantidad_cosechada

    def cosecha(self):
        return self.__cosecha


class Soldado(Personaje):
    def __init__(self, vida, posicion, velocidad, ataque):
        super().__init__(vida, posicion, velocidad)
        self.__ataque = ataque

    def atacar(self, otro_soldado):
        otro_soldado.recibir_ataque(self.__ataque)
