from app.juego_de_rol import Personaje, Soldado, Campesino, Posicion, Adelante, Atras
import pytest


def test_personaje_con_atributos():
    vida = 100
    posicion = Posicion(0)
    velocidad = 5

    assert Personaje(vida, posicion, velocidad)


def test_personaje_recibe_ataca():
    vida = 100
    posicion = Posicion(0)
    velocidad = 5

    personaje = Personaje(vida, posicion, velocidad)

    ataque_recibido = 40

    personaje.recibir_ataque(ataque_recibido)

    assert personaje.vida() == 60


def test_personaje_recibe_ataque_y_se_desborda():
    vida = 100
    posicion = Posicion(0)
    velocidad = 5

    personaje = Personaje(vida, posicion, velocidad)

    with pytest.raises(ValueError):
        ataque_recibido = 140
        personaje.recibir_ataque(ataque_recibido)


def test_personaje_se_puede_mover_adelante():
    vida = 100
    posicion = Posicion(0)
    velocidad = 5

    personaje = Personaje(vida, posicion, velocidad)

    direccion = Adelante(posicion)

    personaje.mover(direccion)

    assert personaje.posicion() == Posicion(5)


def test_personaje_se_puede_mover_atras():
    vida = 100
    posicion = Posicion(10)
    velocidad = 5

    personaje = Personaje(vida, posicion, velocidad)

    direccion = Atras(posicion)

    personaje.mover(direccion)

    assert personaje.posicion() == Posicion(5)


def test_hacer_un_soldado():
    vida = 100
    posicion = Posicion(10)
    velocidad = 5
    ataque = 20

    assert Soldado(vida, posicion, velocidad, ataque)


def test_soldado_ataca():
    vida = 100
    posicion = Posicion(10)
    velocidad = 5
    ataque = 20

    un_soldado = Soldado(vida, posicion, velocidad, ataque)

    otro_soldado = Soldado(vida, posicion, velocidad, ataque)

    un_soldado.atacar(otro_soldado)

    assert otro_soldado.vida() == 80


def test_hacer_un_campesino():
    vida = 100
    posicion = Posicion(10)
    velocidad = 5
    cosecha = 0

    assert Campesino(vida, posicion, velocidad, cosecha)


def test_campesino_cosecha():
    vida = 100
    posicion = Posicion(10)
    velocidad = 5
    cosecha = 0

    campesino = Campesino(vida, posicion, velocidad, cosecha)

    # el metodo cosechar() cosecha 1

    assert campesino.cosechar() == campesino.cosecha()
