''' Este archivo calcula el ganador del envido entre dos jugadores
'''
from app.ejercicio_envido import calcular_envido


def obtener_cartas(mano):
    return mano.split(';')[0], mano.split(';')[1], mano.split(';')[2]


def es_invalida(carta):
    ''' Esta funcion determina si es o no una carta invalida
        Si no encuenta un numero valido y un palo valido entonces la carta es invalida
    '''
    numeros = ['1', '2', '3', '4', '5', '6', '7', '10', '11', '12']
    palos = ['p', 'e', 'c', 'o']
    validacion_numeros = False
    validacion_palos = False
    for numero in numeros:
        if numero == carta.split('|')[0]:
            validacion_numeros = True
            break

    for palo in palos:
        if palo == carta.split('|')[1]:
            validacion_palos = True
            break

    return not (validacion_numeros and validacion_palos)


def calcular_ganador(jugador1, jugador2):
    j1_carta1, j1_carta2, j1_carta3 = obtener_cartas(jugador1)
    j2_carta1, j2_carta2, j2_carta3 = obtener_cartas(jugador2)

    if es_invalida(j1_carta1) or es_invalida(j1_carta2) or es_invalida(j1_carta3):
        return -1

    if es_invalida(j2_carta1) or es_invalida(j2_carta2) or es_invalida(j2_carta3):
        return -1

    envido_jugador1 = calcular_envido(j1_carta1, j1_carta2, j1_carta3)
    envido_jugador2 = calcular_envido(j2_carta1, j2_carta2, j2_carta3)

    if envido_jugador1 >= envido_jugador2:
        return 1

    if envido_jugador1 < envido_jugador2:
        return 2

    return 0
