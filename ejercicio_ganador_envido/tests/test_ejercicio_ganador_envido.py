from app.ejercicio_ganador_envido import calcular_ganador


def test_con_ganador_jugador1():
    jugador_1 = "1|o;10|o;3|c"
    jugador_2 = "1|p;10|e;4|c"
    ganador = calcular_ganador(jugador_1, jugador_2)
    assert ganador == 1


def test_con_ganador_jugador2():
    jugador_1 = "1|o;10|o;3|c"
    jugador_2 = "1|p;10|e;3|p"
    ganador = calcular_ganador(jugador_1, jugador_2)
    assert ganador == 2


def test_con_empate():
    jugador_1 = "1|o;10|o;3|c"
    jugador_2 = "1|c;10|c;3|p"
    ganador = calcular_ganador(jugador_1, jugador_2)

    assert ganador == 1


def test_con_cartas_invalidas():
    jugador_1 = "1|o;10|o;3|x"
    jugador_2 = "1|p;10|e;3|p"
    ganador = calcular_ganador(jugador_1, jugador_2)

    assert ganador == -1
