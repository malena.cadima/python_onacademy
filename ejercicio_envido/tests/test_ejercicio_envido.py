''' Hola,
    Este archivo testea ejercicio_envido.py
    Los test se dividen en cinco secciones.
    1. Con tres cartas menores o iguales a 7
    2. Con tres cartas negras o figura
    3. Con cartas negras y menores a 7
    4. Con tres cartas del mismo palo
    5. Con tres cartas de diferentes palos
'''

from app.ejercicio_envido import calcular_envido


# 1. Con tres cartas menores o iguales a 7

def test_con_primera_y_segunda_cartas_blancas_mismo_palo():
    assert calcular_envido('1|p', '6|p', '6|o') == 27


def test_con_primera_y_tercera_cartas_blancas_mismo_palo():
    assert calcular_envido('7|e', '6|c', '6|e') == 33


def test_con_segunda_y_tercera_cartas_blancas_mismo_palo():
    assert calcular_envido('3|e', '2|c', '3|c') == 25


# 2. Con tres cartas negras o figura (SOLO NEGRAS)

def test_con_primera_y_segunda_cartas_negras_mismo_palo():
    assert calcular_envido('11|p', '12|p', '6|o') == 20


def test_con_primera_y_tercera_cartas_negras_mismo_palo():
    assert calcular_envido('10|e', '11|c', '12|e') == 20


def test_con_segunda_y_tercera_cartas_negras_mismo_palo():
    assert calcular_envido('12|e', '11|c', '10|c') == 20


# 3. Con cartas negras y menores a 7

def test_primera_y_segunda_carta_negra_y_blanca():
    assert calcular_envido('11|p', '2|p', '6|o') == 22


def test_primera_y_tercera_carta_blanca_y_negra():
    assert calcular_envido('1|e', '11|c', '12|e') == 21


def test_segunda_y_tercera_carta_blanca_y_negra():
    assert calcular_envido('12|e', '3|c', '10|c') == 23


# 4. Con tres cartas del mismo palo

def test_tres_palos_iguales_dos_blancas_y_una_negra():
    assert calcular_envido('12|p', '5|p', '3|p') == 28


def test_tres_palos_iguales_dos_negras_y_una_blanca():
    assert calcular_envido('12|p', '11|p', '3|p') == 23


def test_tres_palos_iguales_tres_negras():
    assert calcular_envido('12|p', '10|p', '11|p') == 20


# 5. Con tres cartas de diferentes palos

def test_palos_diferentes_con_dos_negras_y_una_blanca():
    assert calcular_envido('12|p', '11|c', '3|o') == 3


def test_palos_diferentes_con_tres_negras():
    assert calcular_envido('12|o', '11|c', '10|p') == 0


def test_palos_diferentes_con_tres_blancas():
    assert calcular_envido('2|p', '7|c', '3|o') == 7
