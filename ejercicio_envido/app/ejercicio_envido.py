''' Bienvenido!

En este archivo esta la función calcular_envido que llama a otras 7 funciones.
Todas estan divididas de la siguiente manera:

- Funciones que reciben una carta y retornan un dato:
    - valor_de_carta
    - palo_de_carta

- Funciones Booleanas
    - hay_tres_palos_iguales
    - hay_dos_palos_iguales

- Funciones para calcular cada caso del envido:
    - calcular_envido_con_tres_palos_iguales
    - calcular_envido_con_dos_palos_iguales
    - calcular_envido_palos_diferentes

- Funcion que calcula el valor del envido a partir de tres cartas.
    - calcular_envido

'''


# Funciones que reciben una carta y retornan un dato

def valor_de(carta):
    '''Funcion que recibe una carta y devuelve su valor para el envido'''
    return int(carta.split('|')[0]) if (int(carta.split('|')[0]) <= 7) else 0


def palo_de(carta):
    ''' Funcion que recibe una carta y devuelve su palo'''
    return carta.split('|')[1]


# Funciones Booleanas

def hay_tres_palos_iguales(carta1, carta2, carta3):
    ''' Función que recibe tres cartas
        Si los tres palos son iguales = TRUE
        SI los tres palos no son iguales = FALSE
    '''
    return palo_de(carta1) == palo_de(carta2) == palo_de(carta3)


def hay_dos_palos_iguales(carta1, carta2, carta3):
    ''' Funcion que recibe tres cartas
        Si dos de las tres cartas tienen el mismo palo, entonces retorna TRUE
        Si no encuentra dos cartas del mismo palo, entonces retorna FALSE
    '''
    palo_c1 = palo_de(carta1)
    palo_c2 = palo_de(carta2)
    palo_c3 = palo_de(carta3)

    return palo_c1 == palo_c2 or palo_c1 == palo_c3 or palo_c2 == palo_c3


# Funciones para calcular cada caso del envido


def calcular_puntaje_del_envido(un_valor, otro_valor):
    return 20 + un_valor + otro_valor


def calcular_envido_tres_palos_iguales(carta1, carta2, carta3):
    puntaje1 = calcular_puntaje_del_envido(valor_de(carta1), valor_de(carta2))
    puntaje2 = calcular_puntaje_del_envido(valor_de(carta1), valor_de(carta3))
    puntaje3 = calcular_puntaje_del_envido(valor_de(carta2), valor_de(carta3))

    if puntaje1 > puntaje2 and puntaje1 > puntaje3:
        return puntaje1
    if puntaje2 > puntaje1 and puntaje2 > puntaje3:
        return puntaje2
    return puntaje3


def calcular_envido_dos_palos_iguales(carta1, carta2, carta3):
    palo_carta1 = palo_de(carta1)
    palo_carta2 = palo_de(carta2)
    palo_carta3 = palo_de(carta3)

    if palo_carta1 == palo_carta2:
        return calcular_puntaje_del_envido(valor_de(carta1), valor_de(carta2))

    if palo_carta1 == palo_carta3:
        return calcular_puntaje_del_envido(valor_de(carta1), valor_de(carta3))

    if palo_carta2 == palo_carta3:
        return calcular_puntaje_del_envido(valor_de(carta2), valor_de(carta3))

    return 0


def calcular_envido_palos_diferentes(carta1, carta2, carta3):
    valor_carta1 = valor_de(carta1)
    valor_carta2 = valor_de(carta2)
    valor_carta3 = valor_de(carta3)

    if valor_carta1 > valor_carta2 and valor_carta1 > valor_carta3:
        return valor_carta1
    if valor_carta2 > valor_carta1 and valor_carta2 > valor_carta3:
        return valor_carta2
    return valor_carta3


# Funcion que calcula el valor del envido a partir de tres cartas
def calcular_envido(carta1, carta2, carta3):
    ''' Funcion que recibe tres cartas
        Retorna el puntaje del envido dependiendo del caso
    '''
    if hay_tres_palos_iguales(carta1, carta2, carta3):
        puntaje = calcular_envido_tres_palos_iguales(carta1, carta2, carta3)
        return puntaje

    if hay_dos_palos_iguales(carta1, carta2, carta3):
        puntaje = calcular_envido_dos_palos_iguales(carta1, carta2, carta3)
        return puntaje

    puntaje = calcular_envido_palos_diferentes(carta1, carta2, carta3)
    return puntaje
