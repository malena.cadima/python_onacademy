from flask import Flask, render_template, request
from app.envido import NumeroCarta, Palo, Carta, Mano


app = Flask(__name__)


@app.route('/')
def index():
    palos = Palo('espada').obtener_palos_disponibles()
    numeros = NumeroCarta(1).obtener_numeros_disponibles()
    cartas = ['Carta 1', 'Carta 2', 'Carta 3']

    return render_template("index.html", palos=palos, numeros=numeros, cartas=cartas)


@app.route('/calcular_envido', methods=['POST'])
def calcular_envido():
    try:
        palo1 = Palo(str(request.form['palo1']))
        numero1 = NumeroCarta(int(request.form['numero1']))
        carta1 = Carta(palo1, numero1)

        palo2 = Palo(str(request.form['palo2']))
        numero2 = NumeroCarta(int(request.form['numero2']))
        carta2 = Carta(palo2, numero2)

        palo3 = Palo(str(request.form['palo3']))
        numero3 = NumeroCarta(int(request.form['numero3']))
        carta3 = Carta(palo3, numero3)

        mano = Mano(carta1, carta2, carta3)

        valor_envido = mano.calcular_envido()

        return render_template('resultado_envido.html', valor_envido=valor_envido)

    except ValueError:

        return render_template('error.html', mensaje='Error Cartas Iguales'), 400
