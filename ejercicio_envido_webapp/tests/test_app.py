from app.app import app


def test_index_get():
    response = app.test_client().get('/')

    assert response.status_code == 200
    assert 'Calcular Envido' in response.get_data(as_text=True)


def test_index_get_cartas():
    response = app.test_client().get('/')

    assert response.status_code == 200
    assert 'Carta 1' in response.get_data(as_text=True)
    assert 'Carta 2' in response.get_data(as_text=True)
    assert 'Carta 3' in response.get_data(as_text=True)


def test_index_get_palos_de_cartas():
    response = app.test_client().get('/')

    assert response.status_code == 200
    assert 'espada' in response.get_data(as_text=True)
    assert 'oro' in response.get_data(as_text=True)
    assert 'palo' in response.get_data(as_text=True)
    assert 'copa' in response.get_data(as_text=True)


def test_index_get_numero_de_cartas():
    response = app.test_client().get('/')

    assert response.status_code == 200
    assert '1' in response.get_data(as_text=True)
    assert '2' in response.get_data(as_text=True)
    assert '3' in response.get_data(as_text=True)
    assert '4' in response.get_data(as_text=True)
    assert '5' in response.get_data(as_text=True)
    assert '6' in response.get_data(as_text=True)
    assert '7' in response.get_data(as_text=True)
    assert '10' in response.get_data(as_text=True)
    assert '11' in response.get_data(as_text=True)
    assert '12' in response.get_data(as_text=True)


def test_error_cartas_iguales():
    response = app.test_client().post('/calcular_envido', data={
        'palo1': 'espada',
        'numero1': '1',

        'palo2': 'espada',
        'numero2': '1',

        'palo3': 'oro',
        'numero3': '1'
    })

    assert response.status_code == 400
