from app.envido import NumeroCarta, Palo, Carta, Mano
import pytest


def test_hacer_una_carta_correcta():
    numero_uno = NumeroCarta(1)
    palo_oro = Palo("oro")
    una_carta = Carta(palo_oro, numero_uno)

    assert una_carta.numero == numero_uno and una_carta.palo == palo_oro


def test_hacer_una_carta_con_numero_invalido():

    palo_oro = Palo('oro')

    with pytest.raises(ValueError):
        numero_invalido = NumeroCarta(8)
        Carta(palo_oro, numero_invalido)


def test_hacer_una_carta_con_palo_invalido():
    numero_dos = NumeroCarta(2)

    with pytest.raises(ValueError):
        palo_invalido = Palo('diamante')
        Carta(palo_invalido, numero_dos)


def test_calcular_envido_con_palos_iguales_y_sin_figuras():
    numero_uno = NumeroCarta(1)
    palo_oro = Palo("oro")
    una_carta = Carta(palo_oro, numero_uno)

    numero_dos = NumeroCarta(2)
    otra_carta = Carta(palo_oro, numero_dos)

    assert una_carta.calcular_envido_con(otra_carta) == 23


def test_calcular_envido_con_palos_diferentes():
    numero_uno = NumeroCarta(1)
    palo_oro = Palo("oro")
    una_carta = Carta(palo_oro, numero_uno)

    numero_dos = NumeroCarta(2)
    palo_espada = Palo('espada')
    otra_carta = Carta(palo_espada, numero_dos)

    assert una_carta.calcular_envido_con(otra_carta) == 2


def test_calcular_envido_con_cartas_negras():
    numero_uno = NumeroCarta(11)
    palo_oro = Palo("oro")
    una_carta = Carta(palo_oro, numero_uno)

    numero_dos = NumeroCarta(2)
    palo_espada = Palo('espada')
    otra_carta = Carta(palo_espada, numero_dos)

    assert una_carta.calcular_envido_con(otra_carta) == 2


def test_calcular_envido_de_mano():
    numero_uno = NumeroCarta(1)
    palo_oro = Palo("oro")
    una_carta = Carta(palo_oro, numero_uno)

    numero_dos = NumeroCarta(2)
    otra_carta = Carta(palo_oro, numero_dos)

    numero_dos = NumeroCarta(2)
    palo_copa = Palo("copa")
    otra_carta_de_otro_palo = Carta(palo_copa, numero_dos)

    mano = Mano(una_carta, otra_carta, otra_carta_de_otro_palo)

    assert mano.calcular_envido() == 23


def test_calcular_envido_de_mano_con_figura():
    numero_uno = NumeroCarta(11)
    palo_oro = Palo("oro")
    una_carta = Carta(palo_oro, numero_uno)

    numero_dos = NumeroCarta(2)
    otra_carta = Carta(palo_oro, numero_dos)

    numero_dos = NumeroCarta(2)
    palo_copa = Palo("copa")
    otra_carta_de_otro_palo = Carta(palo_copa, numero_dos)

    mano = Mano(una_carta, otra_carta, otra_carta_de_otro_palo)

    assert mano.calcular_envido() == 22


def test_calcular_envido_de_mano_con_dos_figura():
    numero_uno = NumeroCarta(11)
    palo_oro = Palo("oro")
    una_carta = Carta(palo_oro, numero_uno)

    numero_dos = NumeroCarta(12)
    otra_carta = Carta(palo_oro, numero_dos)

    numero_dos = NumeroCarta(2)
    palo_copa = Palo("copa")
    otra_carta_de_otro_palo = Carta(palo_copa, numero_dos)

    mano = Mano(una_carta, otra_carta, otra_carta_de_otro_palo)

    assert mano.calcular_envido() == 20


def test_calcular_envido_de_mano_con_flor():
    numero_uno = NumeroCarta(11)
    palo_oro = Palo("oro")
    una_carta = Carta(palo_oro, numero_uno)

    numero_dos = NumeroCarta(12)
    otra_carta = Carta(palo_oro, numero_dos)

    numero_dos = NumeroCarta(2)
    otra_carta_de_otro_palo = Carta(palo_oro, numero_dos)

    mano = Mano(una_carta, otra_carta, otra_carta_de_otro_palo)

    assert mano.calcular_envido() == 22
