class ErrorNoHayLetra(ValueError):
    pass


class Trabado:
    def __init__(self, letra, cantidad=0):
        self.__letra = letra
        self.__cantidad = cantidad
        self.validar_letra()

    def procesar(self, palabra):
        indice = palabra.find(self.__letra)

        if indice > 0:
            trabar = self.__letra * self.__cantidad
            palabra = palabra[0:indice] + trabar + palabra[indice + self.__cantidad:]

        return palabra

    def validar_letra(self):
        if self.__letra == '':
            raise ErrorNoHayLetra('No paso ninguna letra')
