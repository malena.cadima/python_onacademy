from flask import Flask, jsonify, request
from flask.helpers import make_response
from app.trabado import Trabado, ErrorNoHayLetra


api = Flask(__name__)


@api.route('/trabado', methods=['POST'])
def inicio_de_trabado():
    datos = request.get_json()

    letra = datos.get('letra')
    cantidad = int(datos.get('cantidad'))
    palabra = datos.get('palabra')

    try:
        trabado = Trabado(letra, cantidad)
    except ErrorNoHayLetra:
        respuesta = {
            "error": "No ingreso una letra"
        }
        return make_response(jsonify(respuesta), 400)

    palabra_procesada = trabado.procesar(palabra)

    respuesta = {
        'letra': letra,
        'cantidad': cantidad,
        'palabra': palabra,
        'palabra_procesada': palabra_procesada
    }

    return make_response(jsonify(respuesta), 200)
