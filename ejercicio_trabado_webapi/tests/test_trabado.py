from app.trabado import Trabado, ErrorNoHayLetra
import pytest


def test_trabado_conoce_que_y_cuantas_veces_trabar():
    assert Trabado('z', 4)


def test_trabado_con_proceso_simple():
    trabado = Trabado('z', 1)

    assert trabado.procesar('lapiz') == 'lapiz'


def test_trabado_con_proceso_medio():
    trabado = Trabado('z', 3)

    assert trabado.procesar('lapiz') == 'lapizzz'


def test_error_no_pasa_letra():
    with pytest.raises(ErrorNoHayLetra):
        assert Trabado('', 3)
