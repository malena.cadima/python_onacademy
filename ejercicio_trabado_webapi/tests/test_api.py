from app.api import api
import json


def test_post_trabar_palabra():
    response = api.test_client().post('/trabado', data=json.dumps({
        "letra": "a",
        "cantidad": 3,
        "palabra": "hola",
    }), content_type='application/json'
    )
    assert response.status_code == 200
    assert response.json['palabra_procesada'] == 'holaaa'


def test_post_trabar_palabra_error():
    response = api.test_client().post('/trabado', data=json.dumps({
        "letra": "",
        "cantidad": 3,
        "palabra": "hola",
    }), content_type='application/json'
    )
    assert response.status_code == 400
