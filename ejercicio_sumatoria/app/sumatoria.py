#pylint: disable = C0114
def calcular_sumatoria(numero):
    '''Entra un numero a la funcion y retorna la sumatoria del mismo'''
    sumatoria = 0
    for i in range(numero+1):
        sumatoria += i
    return sumatoria
