from app.sumatoria import calcular_sumatoria

def test_1():
    assert calcular_sumatoria(1) == 1

def test_2():
    assert calcular_sumatoria(3) == 6
