
Hacer un trabado se inicializa con una numero y una letra y cada vez que procesa una cadena que tienen esa letra, la repite.
Si el trabado es inicializado incorrectamente debe lanzar una excepción del tipo InicializacionError.

Ejemplo 1:

trabado = Trabado("z", 4)

trabado.procesar("za") => "zzzza"



Ejemplo 2:

trabado = Trabado("a", 3)

trabado.procesar("hola") => "holaaaa"
