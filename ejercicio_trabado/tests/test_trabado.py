from app.trabado import Trabado


def test_trabado_conoce_que_y_cuantas_veces_trabar():
    assert Trabado('z', 4)


def test_trabado_con_proceso_simple():
    trabado = Trabado('z', 1)

    assert trabado.procezar('lapiz') == 'lapiz'


def test_trabado_con_proceso_medio():
    trabado = Trabado('z', 3)

    assert trabado.procezar('lapiz') == 'lapizzz'
