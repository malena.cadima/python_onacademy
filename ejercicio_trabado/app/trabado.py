class Trabado:
    def __init__(self, letra, cantidad):
        self.__letra = letra
        self.__cantidad = cantidad

    def procezar(self, palabra):
        indice = palabra.find(self.__letra)

        if indice > 0:
            trabar = self.__letra * self.__cantidad
            palabra = palabra[0:indice] + trabar + palabra[indice + self.__cantidad:]

        return palabra
