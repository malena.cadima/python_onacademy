# pylint: disable=R0903
class NumeroCarta:
    def __init__(self, numero):
        self.__numero = numero
        self.validar_numero()

    def obtener_numero(self):
        return self.__numero

    def validar_numero(self):
        if not isinstance(self.__numero, int):
            raise ValueError('Esto no es un numero.')
        if self.__numero not in (1, 2, 3, 4, 5, 6, 7, 10, 11, 12):
            raise ValueError('Numero invalido.')

    def __lt__(self, otro_numero):
        return self.__numero < otro_numero.obtener_numero()

    def __eq__(self, otro_numero):
        return self.__numero == otro_numero.obtener_numero()

    def valor_envido(self):
        if self.__numero > 7:
            return 0
        return self.__numero

    def __add__(self, otro_numero):
        return self.valor_envido() + otro_numero.valor_envido()


class Palo:
    def __init__(self, palo):
        self.__palo = palo.lower()
        self.validar_palo()

    def validar_palo(self):
        if not isinstance(self.__palo, str):
            raise ValueError('Esto no es un palo')
        if self.__palo not in ('oro', 'espada', 'palo', 'copa'):
            raise ValueError('palo no valido. Tiene que ser "oro", "espada", "palo" o "copa".')

    def obtener_palo(self):
        return self.__palo

    def __eq__(self, otro_palo):
        return self.__palo == otro_palo.obtener_palo()


class Carta:
    def __init__(self, palo, numero):
        self.palo = palo
        self.numero = numero

    def calcular_envido_con(self, otra_carta):
        '''Si son del mismo palo, entonces suma el valor de las cartas para el envido.
        Sino se entiende que son de diferentes palos, entonces retorna la carta de mayor valor'''

        carta1_valor = self.numero.valor_envido()
        carta2_valor = otra_carta.numero.valor_envido()

        if self.palo == otra_carta.palo:
            puntaje = carta1_valor + carta2_valor + 20
            return puntaje

        puntaje = max(carta1_valor, carta2_valor)
        return puntaje


class Mano:
    def __init__(self, carta1, carta2, carta3):
        self.carta2 = carta2
        self.carta1 = carta1
        self.carta3 = carta3

    def calcular_envido(self):
        puntaje1 = self.carta1.calcular_envido_con(self.carta2)
        puntaje2 = self.carta1.calcular_envido_con(self.carta3)
        puntaje3 = self.carta2.calcular_envido_con(self.carta3)

        puntaje = max(puntaje1, puntaje2, puntaje3)

        return puntaje
