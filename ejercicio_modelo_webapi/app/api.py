from flask import Flask, jsonify
from flask.helpers import make_response


api = Flask(__name__)


@api.route('/')
def saludar():
    response = {"mensaje": "hola!"}
    return make_response(jsonify(response), 200)
