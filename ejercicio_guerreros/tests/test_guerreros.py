from app.guerreros import Guerrero, Paladin, Mago


def test_guerrero_gana_contra_otro_guerrero():
    un_guerrero = Guerrero()
    otro_guerrero = Guerrero()

    assert un_guerrero.pelear_contra(otro_guerrero) is True

    assert un_guerrero.esta_vivo is True
    assert otro_guerrero.esta_vivo is False


def test_un_paladin_gana_contra_un_guerrero():
    un_guerrero = Guerrero()
    un_paladin = Paladin()

    un_paladin.pelear_contra(un_guerrero)

    assert un_paladin.esta_vivo is True
    assert un_guerrero.esta_vivo is False


def test_un_guerrero_pierde_contra_un_paladin():
    un_guerrero = Guerrero()
    un_paladin = Paladin()

    assert un_guerrero.pelear_contra(un_paladin) is False

    assert un_paladin.esta_vivo is True
    assert un_guerrero.esta_vivo is False


def test_un_paladin_gana_contra_un_mago():
    un_paladin = Paladin()
    un_mago = Mago()

    un_paladin.pelear_contra(un_mago)

    assert un_paladin.esta_vivo is True
    assert un_mago.esta_vivo is False


def test_un_mago_gana_contra_un_paladin():
    un_paladin = Paladin()
    un_mago = Mago()

    un_mago.pelear_contra(un_paladin)

    assert un_paladin.esta_vivo is False
    assert un_mago.esta_vivo is True


def test_un_mago_gana_contra_un_guerrero():
    un_mago = Mago()
    un_guerrero = Guerrero()

    un_mago.pelear_contra(un_guerrero)

    assert un_mago.esta_vivo is True
    assert un_guerrero.esta_vivo is False


def test_un_guerrero_gana_contra_un_mago():
    un_mago = Mago()
    un_guerrero = Guerrero()

    un_guerrero.pelear_contra(un_mago)

    assert un_mago.esta_vivo is False
    assert un_guerrero.esta_vivo is True


def test_cantidad_de_victorias_del_paladin():
    un_paladin = Paladin()
    un_mago = Mago()

    un_paladin.pelear_contra(un_mago)

    assert un_paladin.cantidad_victorias == 1
