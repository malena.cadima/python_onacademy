Consigna
========

En este ejercicio vamos a programar la logica de un juego de batallas muy simple.
Para eso, vamos a tener que crear una clase *Luchador*, que servira de base para los diferentes tipos de luchadores.
Esta clase deberá tener dos atributos: *vida* y *ataque*, y también deberá tener un método *pelear_contra* que reciba como parametro otro *Luchador*.
El método *pelear_contra* deberá devolver True si el Luchador desde el cual se llama es el ganador, y False en el caso de que el Luchador que se recibe como parámetro sea el ganador.
Para encontrar un ganador, los Luchadores seran sometidos a una pelea por turnos. El Luchador desde el cual se llama al metodo será el atacante en el primer turno, y le descontará su *ataque* a la *vida* del otro luchador. En el segundo turno, la operación será la misma, pero esta vez el Luchador recibido por parámetro será el atacante. La pelea se desarrolla de esta manera sucesivamente hasta que uno de los Luchadores muera (no tenga más puntos de vida). El Luchador que no haya muerto será el ganador.


Haciendo uso de *Herencia*, crearemos 3 tipos distintos de Luchadores:


*Guerrero*: 50 puntos de vida, 7 puntos de ataque.
*Paladin*: 40 puntos de vida, 10 puntos de ataque.
*Mago*: 25 puntos de vida, 15 puntos de ataque.



Bonus!
======


En algún punto necesitamos saber si cualquiera de nuestros luchadores está vivo o no. Se habrán dado cuenta que la única forma en este momento es consultar directamente por la vida (si es mayor o menor a cero).
Podemos simplificar esta consulta, y darle un nombre especifico al que podemos acceder facilmente desde los objetos usando *propiedades*.


Una propiedad en un objeto es simplemente un método. La diferencia es que es accedido exactamente igual que los atributos, es decir, sin los paréntesis que usamos normalmente para llamar a métodos o funciones.





Ejemplo:
========
    *class Circulo:*

    def __init__(self, radio):

    self.radio = radio




    @property

    def area(self):

    pi = 3.14159265

    return pi * self.radio * self.radio





    circulo1 = Circulo(10)

    print(circulo1.area) # Va a mostrar 314.159265, el área de un círculo de 10 unidades.


Crear una propiedad llamada *esta_vivo* en la clase base que devuelva True si un luchador aun tiene puntos de vida y False en caso contrario.
Probablemente en la lógica de la pelea le puedan dar un buen uso a esta nueva propiedad.



Segundo Bonus!
==============


Ahora queremos también saber los enemigos derrotados por cada luchador, y tener una forma facil de saber cuantos derrotó después de varias peleas.
Para esto, deberemos crear un *set* llamado *enemigos_derrotados* en la clase *Luchador*. Durante cada pelea, se deberá agregar (*add*) al set del luchador victorioso, el otro luchador (el perdedor).
Ademas, tendremos que crear otra *property* llamada *cantidad_de_victorias*, que nos devuelva el conteo de los enemigos que ese luchador haya derrotado.
