class Luchador:
    def __init__(self):
        self.vida = None
        self.ataque = None
        self.enemigos_derrotados = set()

    def pelear_contra(self, otro_luchador):

        while self.esta_vivo and otro_luchador.esta_vivo:
            otro_luchador.vida = otro_luchador.vida - self.ataque

            if not otro_luchador.esta_vivo:
                self.agregar_enemigo_derrotado(otro_luchador)
                return True

            self.vida = self.vida - otro_luchador.ataque

            if not self.esta_vivo:
                otro_luchador.agregar_enemigo_derrotado(self)
                return False

    @property
    def esta_vivo(self):
        return self.vida > 0

    def agregar_enemigo_derrotado(self, otro_luchador):
        self.enemigos_derrotados.add(otro_luchador)

    @property
    def cantidad_victorias(self):
        return len(self.enemigos_derrotados)


class Guerrero(Luchador):
    def __init__(self):
        super().__init__()
        self.vida = 50
        self.ataque = 7


class Paladin(Luchador):
    def __init__(self):
        super().__init__()
        self.vida = 40
        self.ataque = 10


class Mago(Luchador):
    def __init__(self):
        super().__init__()
        self.vida = 25
        self.ataque = 15
