class Papel:
    def __init__(self):
        self.__texto = ''

    def escribir(self, mensaje):
        self.__texto += str(mensaje)
        return self.__texto

    def __str__(self):
        return self.__texto


class Birome:
    def __init__(self, tinta):
        self._tinta = tinta

    def obtener_tinta(self):
        return self._tinta

    def hay_tinta(self):
        return self.obtener_tinta() > 0

    def escribir(self, mensaje, papel):
        cantidad_de_tinta_por_letra = 2

        for letra in mensaje:

            if not self.hay_tinta():
                raise ValueError('Ups... necesitamos otra birome')

            papel.escribir(letra)
            self._tinta -= cantidad_de_tinta_por_letra

        return str(papel)


class Marcador(Birome):
    def recargar(self, cantidad_tinta):
        self._tinta += cantidad_tinta
