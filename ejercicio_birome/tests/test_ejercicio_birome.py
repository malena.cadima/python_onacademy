from app.ejercicio_birome import Papel, Birome, Marcador
import pytest


def obtener_mensaje():
    return 'Mensaje de ejemplo'


def obtener_tinta():
    return 100


def test_asegurar_que_papel_reciba_un_texto():
    mensaje = obtener_mensaje()
    papel = Papel()
    papel.escribir(mensaje)

    assert str(papel) == mensaje


def test_birome_existe():
    tinta = obtener_tinta()
    birome = Birome(tinta)

    assert birome.hay_tinta() is True


def test_birome_escribe_en_papel():
    papel = Papel()

    mensaje = obtener_mensaje()

    tinta = obtener_tinta()
    birome = Birome(tinta)

    birome.escribir(mensaje, papel)

    assert str(papel) == mensaje


def test_birome_se_queda_sin_tinta():
    papel = Papel()

    tinta = 1
    birome = Birome(tinta)

    mensaje = 'Mensaje muy largo. Tinta insuficiente'

    with pytest.raises(ValueError):
        birome.escribir(mensaje, papel)


def test_existe_marcador():
    tinta = obtener_tinta()
    marcador = Marcador(tinta)

    assert marcador.hay_tinta() is True


def test_marcador_escribe_en_papel():
    papel = Papel()

    mensaje = obtener_mensaje()

    tinta = obtener_tinta()
    marcador = Marcador(tinta)

    marcador.escribir(mensaje, papel)

    assert str(papel) == mensaje


def test_se_recarga_tinta_en_el_marcador():
    cantidad_tinta = 0

    marcador = Marcador(cantidad_tinta)

    cantidad_tinta = 100

    marcador.recargar(cantidad_tinta)

    assert marcador.hay_tinta() is True


def test_recargar_tinda_de_marcador():
    cantidad_tinta = 50

    marcador = Marcador(cantidad_tinta)

    cantidad_tinta = 100

    marcador.recargar(cantidad_tinta)

    assert marcador.obtener_tinta() == 150
