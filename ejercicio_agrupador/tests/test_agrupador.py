from app.agrupador import Agrupador


def test_hacer_un_grupo_con_una_persona():
    cantidad_de_personas_por_grupo = 1
    agrupador = Agrupador(cantidad_de_personas_por_grupo)

    agrupador.agregar_nombre('Malena')

    agrupador.armar_grupos()

    assert agrupador.cantidad_de_grupos() == 1


def test_hacer_un_grupo_con_dos_personas():
    cantidad_de_personas_por_grupo = 2
    agrupador = Agrupador(cantidad_de_personas_por_grupo)

    agrupador.agregar_nombre('Malena')
    agrupador.agregar_nombre('Valeria')

    agrupador.armar_grupos()

    assert agrupador.cantidad_de_grupos() == 1


def test_hacer_dos_grupo_con_una_personas():
    cantidad_de_personas_por_grupo = 1
    agrupador = Agrupador(cantidad_de_personas_por_grupo)
    agrupador.agregar_nombre('Malena')
    agrupador.agregar_nombre('Valeria')

    agrupador.armar_grupos()

    assert agrupador.cantidad_de_grupos() == 2


def test_hacer_tres_grupo_de_dos_personas():
    cantidad_de_personas_por_grupo = 2
    agrupador = Agrupador(cantidad_de_personas_por_grupo)

    agrupador.agregar_nombre('Andres')
    agrupador.agregar_nombre('Barbara')
    agrupador.agregar_nombre('Camila')
    agrupador.agregar_nombre('Daniel')
    agrupador.agregar_nombre('Esperanza')
    agrupador.agregar_nombre('Florencia')

    agrupador.armar_grupos()

    assert agrupador.cantidad_de_grupos() == 3
