import random


class Agrupador:
    def __init__(self, cantidad_de_personas_por_grupo):
        self.__cantidad_de_nombres = int(cantidad_de_personas_por_grupo)
        self.__nombres = []
        self.__grupos = []

    def agregar_nombre(self, nombre):
        self.__nombres.append(nombre)

    def obtener_grupos(self):
        return self.__grupos

    def armar_grupos(self):
        self._desordenar_lista_nombres()

        for i in range(0, len(self.__nombres), self.__cantidad_de_nombres):
            self.__grupos.append(self.__nombres[i:i+self.__cantidad_de_nombres])

    def _desordenar_lista_nombres(self):
        random.shuffle(self.__nombres)

    def cantidad_de_grupos(self):
        return len(self.__grupos)
