Desarrollar una aplicación de línea de comandos que reciba un archivo con nombres (uno por línea) y un número X y que agrupe de forma aleatoria los nombres dentro del archivo en grupos de X integrantes.




# caso general
# invocacion: python3 agrupar.py <nombre_archivo_con_nombres> <cantidad_de_personas_por_grupo>
# grupo: nombre, nombre, nombre

# alumnos.txt
# juan
# ana
# maria
# elvio
# roberto
# clara

python3 agrupar.py alumnos.txt 2
grupo 1: roberto, maria
grupo 2: ana, clara
grupo 3: juan, elvio

# Recomendación: comenzar trabajando primero en la lógica de agrupación resolviendo y commiteando los siguientes casos/test. Luego ver de agregar la lógica de aleatoriedad






# caso simple 1
alumnos:juan
cantidad_de_personas_por_grupo:1
resultado: grupo1:juan

# caso simple 2
alumnos:juan,maria
cantidad_de_personas_por_grupo:2
resultado: grupo1:juan,maria

# caso simple 3
alumnos:juan,maria
cantidad_de_personas_por_grupo:1
resultado: grupo1:juan, grupo2:maria
