import sys
from app.agrupador import Agrupador


nombre_archivo_con_nombres = sys.argv[1]
cantidad_de_personas_por_grupo = sys.argv[2]

agrupador = Agrupador(cantidad_de_personas_por_grupo)

with open(nombre_archivo_con_nombres, 'r') as nombres:
    for linea in nombres:
        nombre = linea.rstrip('\n')
        agrupador.agregar_nombre(nombre)

agrupador.armar_grupos()
grupos = agrupador.obtener_grupos()

for numero, grupo in enumerate(grupos):
    print('grupo ', numero, ' : ', ', '.join(grupo))
