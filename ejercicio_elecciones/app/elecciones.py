class Voto:
    def __init__(self, provincia, candidato):
        self.__provincia = provincia
        self.__candidato = candidato

    def provincia(self):
        return self.__provincia

    def candidato(self):
        return self.__candidato


class Eleccion:

    def __init__(self):
        self.__provincias = []
        self.__candidatos = []
        self.__votos = []

    def agregar_provincia(self, nombre_provincia):
        self.__provincias.append(nombre_provincia)

    def agregar_voto(self, nombre_provincia, nombre_candidato):
        if nombre_provincia in self.__provincias:

            if nombre_candidato not in self.__candidatos:
                self.__candidatos.append(nombre_candidato)

            un_voto = Voto(nombre_provincia, nombre_candidato)

            self.__votos.append(un_voto)

    def contar_votos(self, provincia, candidato):
        if provincia not in self.__provincias:
            raise ValueError('Esta provincia no esta en la lista')

        if candidato not in self.__candidatos:
            raise ValueError('Este candidato no esta en la lista')

        contador_votos = 0

        for voto in self.__votos:
            provincia_votada = voto.provincia()
            candidato_votado = voto.candidato()
            if provincia == provincia_votada:
                if candidato == candidato_votado:
                    contador_votos += 1

        return contador_votos

    def resultado_en(self, nombre_provincia):
        if nombre_provincia not in self.__provincias:
            raise ValueError('Esta provincia no esta en la lista')

        auxiliar_para_resultado = []

        for candidato in self.__candidatos:
            votos_del_candidato = self.contar_votos(nombre_provincia, candidato)
            candidato_votos = ':'.join([str(candidato), str(votos_del_candidato)])
            auxiliar_para_resultado.append(candidato_votos)

        auxiliar_para_resultado.sort()
        resultado = ','.join(auxiliar_para_resultado)

        return resultado
