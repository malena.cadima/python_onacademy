Dado un archivo con el detalle de votos de una elección nacional y otro archivo con la configuración de la elección, desarrollar una aplicación que genere un archivo con el resultado de la elección a nivel provincia y a nivel nacional.

# forma de ejecucion
# python3 eleccion_app.py <nombre_archivo_de_configuracion> <nombre_archivo_de_entrada> <nombre_archivo_de_salida>
python3 eleccion_app.py configuracion.txt votos.csv resultado.csv

# configuracion.txt
cordoba
neuquen

# votos.csv
cordoba,rojo
cordoba,rojo
cordoba,azul
neuquen,azul
neuquen,azul
neuquen,rojo
neuquen,blanco

# resultado.csv
cordoba,rojo:2,azul:1,blanco:0
neuquen,azul:2,rojo:1,blanco:1
nacion,rojo:3,azul:3,blanco:1


Utilizar como punto de partida para el ejercicio el código trabajado en clase y que está disponible aquí: https://gitlab.com/onacademy/ejercicios_en_clase/-/tree/master/ejercicio_eleccion (Enlaces a un sitio externo.). Esto implica que hay que trabajar exclusivamente dentro de la clase Eleccion, hay total libertad para crear otras clases/objetos siempre que estos sean utilizando dentro de Eleccion pero sin cambiar los métodos existentes en Eleccion.
