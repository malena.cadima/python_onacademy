import pytest
from app.elecciones import Eleccion


def test_contar_votos_cuando_hay_un_solo_voto():
    eleccion = Eleccion()
    eleccion.agregar_provincia("cordoba")
    eleccion.agregar_voto("cordoba", "rojo")
    assert eleccion.contar_votos("cordoba", "rojo") == 1


def test_resultado_cuando_hay_un_solo_voto():
    eleccion = Eleccion()
    eleccion.agregar_provincia("cordoba")
    eleccion.agregar_voto("cordoba", "rojo")
    resultado = eleccion.resultado_en("cordoba")
    assert resultado == "rojo:1"

def test_contar_votos_cuando_hay_un_varios_votos():
    eleccion = Eleccion()
    eleccion.agregar_provincia("cordoba")
    eleccion.agregar_voto("cordoba", "rojo")
    eleccion.agregar_voto("cordoba", "azul")
    eleccion.agregar_voto("cordoba", "azul")
    assert eleccion.contar_votos("cordoba", "rojo") == 1
    assert eleccion.contar_votos("cordoba", "azul") == 2


# @pytest.mark.xfail # esto indica que ya sabemos que este test va a fallar
def test_resultado_cuando_hay_un_varios_votos():
    eleccion = Eleccion()
    eleccion.agregar_provincia("cordoba")
    eleccion.agregar_voto("cordoba", "rojo")
    eleccion.agregar_voto("cordoba", "azul")
    eleccion.agregar_voto("cordoba", "azul")
    resultado = eleccion.resultado_en("cordoba")
    assert resultado == "azul:2,rojo:1"

