from app.ejercicio_factura import Factura, Item
from datetime import date


def test_tamanio_de_lista_de_items():
    numero_de_factura = '0001'
    fecha_de_factura = date.today().year

    un_item = Item('Item 1', '10', '2')
    otro_item = Item('Item 2', '20', '4')

    una_factura = Factura(numero_de_factura, fecha_de_factura)

    una_factura.agregar_item(un_item)
    una_factura.agregar_item(otro_item)

    numero_de_factura2 = '0002'
    fecha_de_factura2 = date.today().year

    un_item2 = Item('Item 1', '1', '1')

    otra_factura = Factura(numero_de_factura2, fecha_de_factura2)

    otra_factura.agregar_item(un_item2)

    assert una_factura.tamanio_de_lista_de_items() == 2
    assert otra_factura.tamanio_de_lista_de_items() == 1


def test_calcular_subtotal_con_dos_items():
    numero_de_factura = '0001'
    fecha_de_factura = date.today().year

    un_item = Item('Item 1', '10', '2')
    otro_item = Item('Item 2', '20', '4')

    una_factura = Factura(numero_de_factura, fecha_de_factura)

    una_factura.agregar_item(un_item)
    una_factura.agregar_item(otro_item)

    assert una_factura.calcular_subtotal() == 100


def test_calcular_total_con_dos_items():
    numero_de_factura = '0001'
    fecha_de_factura = date.today().year

    un_item = Item('Item 1', '10', '2')
    otro_item = Item('Item 2', '20', '4')

    una_factura = Factura(numero_de_factura, fecha_de_factura)

    una_factura.agregar_item(un_item)
    una_factura.agregar_item(otro_item)

    assert una_factura.calcular_total() == 121


def test_calcular_subtotal_de_factura_con_un_item():
    numero_de_factura = '0001'
    fecha_de_factura = date.today().year

    un_item = Item('Item 1', '1', '1')

    una_factura = Factura(numero_de_factura, fecha_de_factura)

    una_factura.agregar_item(un_item)

    assert una_factura.calcular_subtotal() == 1


def test_calcular_total_de_factura_con_un_item():
    numero_de_factura = '0001'
    fecha_de_factura = date.today().year

    un_item = Item('Item 1', '1', '1')

    una_factura = Factura(numero_de_factura, fecha_de_factura)

    una_factura.agregar_item(un_item)

    assert una_factura.calcular_total() == 1.21
