#!/bin/bash
set -e

python3 -m flake8 --max-line-length 120
python3 -m pytest
python3 -m pylint --disable missing-docstring app/

