# pylint: disable=R0903

class Item():
    def __init__(self, descripcion, precio_unitario, cantidad):
        self.descripcion = descripcion
        self.precio_unitario = precio_unitario
        self.cantidad = cantidad


class Factura():
    def __init__(self, numero, fecha):
        self.numero = numero
        self.fecha = fecha
        self.list_items = []

    def agregar_item(self, nuevo_item):
        self.list_items.append(nuevo_item)

    def calcular_subtotal(self):
        subtotal = 0
        for item in self.list_items:
            subtotal = float(item.precio_unitario) * int(item.cantidad) + subtotal
        return subtotal

    def calcular_total(self):
        total = (self.calcular_subtotal() * 21 / 100) + self.calcular_subtotal()
        return total

    def tamanio_de_lista_de_items(self):
        return len(self.list_items)
