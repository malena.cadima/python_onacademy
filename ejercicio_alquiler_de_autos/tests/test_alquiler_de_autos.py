from app.alquiler_autos import AlquilerDeAutos


''' python app.py <fecha_alquiler> <cuit> <tipo_alquiler> <parametros_alquiler> '''


def test_alquiler_por_3_horas_a_una_persona():
    '''01-alquiler por hora' 20190119 20112223336 h 3 'Importe: 300' '''

    fecha_alquiler = 20190119
    cuit = 20112223336
    tipo_alquiler = 'h'
    parametros_alquiler = 3

    alquiler = AlquilerDeAutos(fecha_alquiler, cuit, tipo_alquiler, parametros_alquiler)

    importe = alquiler.calcular_facturacion()

    assert importe == 300


def test_alquiler_por_1_dia_a_una_persona():
    ''' check '02-alquier por dia' 20190119 20112223336 d 1 'Importe: 2000' '''

    fecha_alquiler = 20190119
    cuit = 20112223336
    tipo_alquiler = 'd'
    parametros_alquiler = 1

    alquiler = AlquilerDeAutos(fecha_alquiler, cuit, tipo_alquiler, parametros_alquiler)

    importe = alquiler.calcular_facturacion()

    assert importe == 2000


def test_alquiler_por_1_dia_a_una_empresa():
    ''' check '03-alquier por dia empresa' 20190119 26112223336 d 1 'Importe: 1900' '''

    fecha_alquiler = 20190119
    cuit = 26112223336
    tipo_alquiler = 'd'
    parametros_alquiler = 1

    alquiler = AlquilerDeAutos(fecha_alquiler, cuit, tipo_alquiler, parametros_alquiler)

    importe = alquiler.calcular_facturacion()

    assert importe == 1900


def test_alquiler_un_miercoles():
    ''' check '04-alquier miercoles' 20190821 20112223336 h 3 'Importe: 250' '''

    fecha_alquiler = 20190821
    cuit = 20112223336
    tipo_alquiler = 'h'
    parametros_alquiler = 3

    alquiler = AlquilerDeAutos(fecha_alquiler, cuit, tipo_alquiler, parametros_alquiler)

    importe = alquiler.calcular_facturacion()

    assert importe == 250
