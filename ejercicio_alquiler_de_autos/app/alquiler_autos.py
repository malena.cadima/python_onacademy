from datetime import datetime
# pylint: disable=R0903
# pylint: disable=W0212


class AlquilerDeAutos:
    def __init__(self, fecha_alquiler, cuit, tipo_alquiler, parametros_alquiler):
        self.fecha = Fecha(fecha_alquiler)
        self.cuit = Cuit(cuit)
        self.tipo_alquiler = TipoDeAlquiler(tipo_alquiler)
        self.parametros_alquiler = Numero(parametros_alquiler)

    def calcular_facturacion(self):
        if self.fecha.es_miercoles():
            subtotal = self.calcular_subtotal()
            total = subtotal - 50

            return total

        if self.cuit.es_empresa():
            subtotal = self.calcular_subtotal()
            total = subtotal - subtotal * 0.05

            return total

        total = self.calcular_subtotal()

        return total

    def calcular_subtotal(self):
        alquiler_por_hora = TipoDeAlquiler('h')
        alquiler_por_dia = TipoDeAlquiler('d')
        alquiler_por_kilometraje = TipoDeAlquiler('k')

        if self.tipo_alquiler == alquiler_por_hora:
            costo = Numero(100)
            facturacion = self.parametros_alquiler * costo

        if self.tipo_alquiler == alquiler_por_dia:
            costo = Numero(2000)
            facturacion = self.parametros_alquiler * costo

        if self.tipo_alquiler == alquiler_por_kilometraje:
            costo = Numero(100)
            facturacion = self.parametros_alquiler * costo

        return facturacion


class Fecha:
    def __init__(self, fecha):
        self.__fecha = int(fecha)

    def crear_fecha(self):
        anio = int(self.__fecha / 10000)
        mes = int(self.__fecha / 100 - anio * 100)
        dia = int(self.__fecha - anio * 10000 - mes * 100)
        fecha = datetime(anio, mes, dia)

        return fecha

    def es_miercoles(self):
        fecha = self.crear_fecha()
        miercoles = 2

        if fecha.weekday() == miercoles:
            return True

        return False


class Cuit:
    def __init__(self, cuit):
        self.__cuit = int(cuit)

    def es_empresa(self):
        cuit_empresa = 26
        return cuit_empresa == int(self.__cuit/1000000000)


class TipoDeAlquiler:
    def __init__(self, tipo_alquiler):
        self.__tipo_alquiler = tipo_alquiler
        self.validar_tipo()

    def validar_tipo(self):
        if self.__tipo_alquiler not in ('h', 'd', 'k'):
            raise ValueError('No es un tipo de alquiler valido  ')

    def __eq__(self, otro):
        return self.__tipo_alquiler == otro.__tipo_alquiler


class Numero:
    def __init__(self, numero):
        self.numero = int(numero)
        self.validar_numero()

    def __mul__(self, costo):
        return self.numero * costo.numero

    def validar_numero(self):
        if not isinstance(self.numero, int):
            raise ValueError('No es numero')
