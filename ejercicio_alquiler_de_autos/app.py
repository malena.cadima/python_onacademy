from app.alquiler_autos import AlquilerDeAutos
import sys


# python app.py <fecha_alquiler> <cuit> <tipo_alquiler> <parametros_alquiler>

fecha_alquiler = sys.argv[1]
cuit = sys.argv[2]
tipo_alquiler = sys.argv[3]
parametros_alquiler = sys.argv[4]

alquiler = AlquilerDeAutos(fecha_alquiler, cuit, tipo_alquiler, parametros_alquiler)

importe = alquiler.calcular_facturacion()

print('Importe: ' + str(importe))
