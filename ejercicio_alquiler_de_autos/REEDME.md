Una empresa de alquiler de autos ofrece distintas modalidades de alquiler:

- Por hora: el cliente debe pagar por cada hora que alquila el auto. El costo es de $ 100 / hora.

- Por día: el cliente paga un monto fijo por día (el día son 24 horas) no importa si el auto se devuelve antes. La cantidad de días debe definir al momento del alquiler. El costo es de $ 2000 / día

- Por kilometraje: el cliente paga un precio fijo por cada kilómetros recorrido durante el período de alquiler. Este tipo de alquiler implica devolución dentro del mismo día de alquiler. El costo $ 100 de base más $ 10/km.



Al mismo tiempo hay una serie de reglas de facturación:

- Los días miércoles todos los alquileres tienen una bonificación de 50 pesos.

- Si quien alquila es una empresa (cuit empieza con 26) tiene un descuento del 5% como parte de la política de fidelización de clientes






Se pide hacer una aplicación de línea de comando que permita registrar alquileres y calcular la correspondiente facturación.

El archivo test.sh contiene un conjunto de pruebas funcionales desde la perspectiva de usuario.


#!/bin/bash
# test.sh

set -e

function check() {
python app.py "$2" "$3" "$4" "$5" | grep "$6" >/dev/null && echo "$1:ok" || echo "$1:error"
}


# python app.py <fecha_alquiler> <cuit> <tipo_alquiler> <parametros_alquiler>

check '01-alquiler por hora' 20190119 20112223336 h 3 'Importe: 300'
check '02-alquier por dia' 20190119 20112223336 d 1 'Importe: 2000'
check '03-alquier por dia empresa' 20190119 26112223336 d 1 'Importe: 1900'
check '04-alquier miercoles' 20190821 20112223336 h 3 'Importe: 250'
