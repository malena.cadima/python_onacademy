class Vagon:
    def __init__(self, personas_paradas, personas_sentadas):
        self.__personas_paradas = personas_paradas
        self.__personas_sentadas = personas_sentadas

    def capacidad(self):
        return self.__personas_paradas + self.__personas_sentadas


class Locomotora:
    def __init__(self, potencia):
        self.__potencia = potencia

    @property
    def potencia(self):
        return self.__potencia


class Tren:
    def __init__(self, locomotora):
        self.__locomotora = locomotora
        self.__vagones = []

    def agregar_vagon(self, vagon):
        self.__vagones.append(vagon)
        self.validar_tren()

    @property
    def cantidad_de_vagones(self):
        return len(self.__vagones)

    def capacidad(self):
        vagones = self.__vagones
        capacidad_total = 0

        for vagon in vagones:
            capacidad_total += vagon.capacidad()

        return capacidad_total

    def validar_tren(self):
        potencia_locomotora = self.__locomotora.potencia

        if potencia_locomotora > 2000:
            raise ValueError('Desborde de potencia en locomotora')

        if potencia_locomotora <= 2000 and potencia_locomotora > 1000:
            if len(self.__vagones) > 7:
                raise ValueError('Desborde por cantidad de vagones mayores a 7')

        if potencia_locomotora <= 1000:
            if len(self.__vagones) > 4:
                raise ValueError('Desborde por cantidad de vagones mayores a 4')
