
Un tren tiene una locomotora y una determinada cantidad de vagones en relación a la potencia de la locomotora.

- Potencia hasta 1000 HP, hasta 4 vagones
- Potencia hasta 2000 HP, hasta 7 vagones

La cantidad de pasajeros que puede transportar el tren está dada por la suma de la capacidad de sus vagones. Cada vagón tiene una capacidad para pasajeros sentados y una capacidad para pasajeros parados.

Se pide modelar este problema y agregar la lógica para determinar la total cantidad de pasajeros de un determinado tren.
