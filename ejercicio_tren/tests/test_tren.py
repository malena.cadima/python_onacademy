from app.tren import Vagon, Locomotora, Tren
import pytest


def test_existe_vagon():
    personas_paradas = 10
    personas_sentadas = 20

    assert Vagon(personas_paradas, personas_sentadas)


def test_existe_una_locomotora():
    potencia = 1000

    assert Locomotora(potencia)


def test_conocer_capacidad_de_vagon():
    personas_paradas = 10
    personas_sentadas = 20

    vagon = Vagon(personas_paradas, personas_sentadas)

    assert vagon.capacidad() == 30


def test_construir_un_tren_solo_con_locomotora():
    potencia = 1000
    locomotora = Locomotora(potencia)

    assert Tren(locomotora)


def test_agregar_un_vagon_al_tren():
    personas_paradas = 10
    personas_sentadas = 20
    vagon = Vagon(personas_paradas, personas_sentadas)

    potencia = 1000
    locomotora = Locomotora(potencia)

    tren = Tren(locomotora)

    tren.agregar_vagon(vagon)

    assert tren.cantidad_de_vagones == 1


def test_conocer_capacidad_de_un_tren_con_tres_vagones():
    personas_paradas = 10
    personas_sentadas = 20
    vagon = Vagon(personas_paradas, personas_sentadas)

    potencia = 1000
    locomotora = Locomotora(potencia)

    tren = Tren(locomotora)

    tren.agregar_vagon(vagon)
    tren.agregar_vagon(vagon)
    tren.agregar_vagon(vagon)

    capacidad_tren = vagon.capacidad() * 3

    assert tren.capacidad() == capacidad_tren


def test_no_se_puede_con_locomotora_1000_y_vagones_5():
    personas_paradas = 10
    personas_sentadas = 20
    vagon = Vagon(personas_paradas, personas_sentadas)

    potencia = 1000
    locomotora = Locomotora(potencia)

    tren = Tren(locomotora)

    tren.agregar_vagon(vagon)
    tren.agregar_vagon(vagon)
    tren.agregar_vagon(vagon)
    tren.agregar_vagon(vagon)

    with pytest.raises(ValueError):
        assert tren.agregar_vagon(vagon)


def test_no_se_puede_con_locomotora_2000_y_vagones_8():
    personas_paradas = 10
    personas_sentadas = 20
    vagon = Vagon(personas_paradas, personas_sentadas)

    potencia = 2000
    locomotora = Locomotora(potencia)

    tren = Tren(locomotora)

    tren.agregar_vagon(vagon)
    tren.agregar_vagon(vagon)
    tren.agregar_vagon(vagon)
    tren.agregar_vagon(vagon)
    tren.agregar_vagon(vagon)
    tren.agregar_vagon(vagon)
    tren.agregar_vagon(vagon)

    with pytest.raises(ValueError):
        assert tren.agregar_vagon(vagon)

