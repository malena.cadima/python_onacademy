'''
Hacer una función que identifique cartas.
Ejemplos:
identificar_carta(“1|p”) = “uno_de_palos”
identificar_carta(“10|e”) = “diez_de_espadas”
identificar_carta(“3|c”) = “tres_de_copas”

'''

def identificar_carta(carta):
    '''
    funcion que recibe un string con la carta y retorna otro string más especifico
    '''

    numero = int(carta.split('|')[0])
    palo = carta.split('|')[1]

    dic_numeros = {
        1 : 'uno',
        2 : 'dos',
        3 : 'tres',
        4 : 'cuatro',
        5 : 'cinco',
        6 : 'seis',
        7 : 'siete',
        8 : 'ocho',
        9 : 'nueve',
        10 : 'diez',
        11 : 'once',
        12 : 'doce'
    }

    dic_palos = {
        'p' : 'palo',
        'e' : 'espada',
        'c' : 'copa',
        'o' : 'oro'
    }

    carta_identificada = dic_numeros[numero] + '_de_' + dic_palos[palo]

    return carta_identificada
