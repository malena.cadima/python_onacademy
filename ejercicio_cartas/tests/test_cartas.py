from app.ejercicio_cartas import identificar_carta


def test_cartas_palo():
    ''' testeo con palo de la funcion identificar_cartas() '''
    assert identificar_carta('5|p') == "cinco_de_palo"


def test_cartas_oro():
    ''' testeo con oro de la funcion identificar_cartas() '''
    assert identificar_carta('10|o') == 'diez_de_oro'


def test_cartas_espada():
    ''' testeo con espada de la funcion identificar_cartas() '''
    assert identificar_carta('3|e') == 'tres_de_espada'


def test_cartas_copa():
    ''' testeo con copa de la funcion identificar_cartas() '''
    assert identificar_carta('12|c') == 'doce_de_copa'
