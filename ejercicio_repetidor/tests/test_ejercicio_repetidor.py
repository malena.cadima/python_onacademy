from app.ejercicio_repetidor import Repetidor
import pytest


def test_repite_cero_veces():
    palabra = Repetidor(0)

    assert palabra.repetir('hola') == ''


def test_repite_una_vez():
    palabra = Repetidor(1)

    assert palabra.repetir('hola') == 'hola'


def test_repite_cinco_veces():
    palabra = Repetidor(5)

    assert palabra.repetir('hola') == 'holaholaholaholahola'


def test_no_admite_numeros_mayores_a_diez():
    with pytest.raises(ValueError):
        Repetidor(11)


def test_no_admite_numeros_negativos():
    with pytest.raises(ValueError):
        Repetidor(-1)


def test_no_admite_dato_no_numerico():
    with pytest.raises(ValueError):
        Repetidor('a')


def test_no_admite_dato_que_no_sea_numero_entero():
    with pytest.raises(ValueError):
        Repetidor(2.3)


def test_no_repite_otro_tipo_de_datos():
    palabra = Repetidor(2)
    with pytest.raises(ValueError):
        palabra.repetir(1234)
