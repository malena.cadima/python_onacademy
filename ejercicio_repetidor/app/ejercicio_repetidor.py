class Repetidor:
    def __init__(self, numero):
        self.numero = self.validar_numero(numero)

    def validar_numero(self, valor):
        if not isinstance(valor, int):
            raise ValueError('"{}" no es un numero entero'.format(valor))
        if valor < 0 or valor > 10:
            raise ValueError('"{}" tiene que ser un numero entre 0 y 10'.format(valor))
        return valor

    def repetir(self, palabra):
        palabra = self.validar_palabra(palabra)
        return palabra * self.numero

    def validar_palabra(self, palabra):
        if not isinstance(palabra, str):
            raise ValueError('"{}" tiene que ser una cadena de caracteres')
        return palabra
