#!/bin/bash
set -e

python3 -m flake8
python3 -m pytest
python3 -m pylint --disable missing-docstring app/

