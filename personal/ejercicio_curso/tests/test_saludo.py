from app.curso import *
from datetime import date
import pytest

def test_estudiante_es_mayor_de_edad():
    # date = AAAAMMDD
    completo_secundario = True
    fecha_nacimiento = date(2000, 1,1)
    estudiante = Estudiante(fecha_nacimiento, completo_secundario)

    assert estudiante.es_mayor_de_edad() is True

def test_curso_solo_adminite_estudiantes_mayores():
    completo_secundario = True
    fecha_nacimiento = date(2000, 1,1)
    estudiante = Estudiante(fecha_nacimiento, completo_secundario)

    cupo = 10
    curso = Curso(cupo)

    assert curso.cantidad_de_estudiantes() == 1

def test_curso_no_adminite_estudiantes_menores():
    completo_secundario = True
    fecha_nacimiento = date(2020, 1,1)
    estudiante = Estudiante(fecha_nacimiento, completo_secundario)

    cupo = 10
    curso = Curso(cupo)

    with pytest.raises(ValueError):
        curso.agregar_estudiante(estudiante)

    assert curso.cantidad_de_estudiantes() == 0





# def test_a():
#     # estudiante, edad, secundario
#     completo_secundario = True
#     fecha_nacimiento = 0
#     estudiante = Estudiante(fecha_nacimiento, completo_secundario)

#     # curso, cupo, estudiantes
#     cupo = 10
#     curso = Curso(cupo)
#     curso.agregar_estudiantes(estudiantes)

#     assert curso.cantidad_de_estudiantes == 1
