class SalaDeEspera(list):
    def llega_nuevo_paciente(self, paciente):
        self.append(paciente)

    def siguiente_paciente(self):
        return self.pop(0)

# class SalaDeEspera():
#     def __init__(self):
#         self.cola_pacientes = []

#     def llega_nuevo_paciente(self, paciente):
#         self.cola_pacientes.append(paciente)

#     def siguiente_paciente(self):
#         return self.cola_pacientes.pop(0)
