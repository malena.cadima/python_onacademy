from app.sala_de_espera import SalaDeEspera


def test_sala_de_espera():
    sala_de_espera = SalaDeEspera()
    sala_de_espera.llega_nuevo_paciente("juan")
    sala_de_espera.llega_nuevo_paciente("maria")
    sala_de_espera.llega_nuevo_paciente("ana")

    assert sala_de_espera.siguiente_paciente() == "juan"
    assert sala_de_espera.siguiente_paciente() == "maria"
    assert sala_de_espera.siguiente_paciente() == "ana"
