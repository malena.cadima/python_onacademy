from app.excepcion import Cuenta
import pytest


def test_la_cuenta_tiene_saldo_inicial_cero():
    cuenta = Cuenta()
    assert cuenta.saldo == 0


def test_cuando_deposito_dinero_aumenta_el_saldo():
    cuenta = Cuenta()
    cuenta.depositar(10)

    assert cuenta.saldo == 10


def test_cuando_retiro_dinero_decremetna_el_saldo():
    cuenta = Cuenta()
    cuenta.depositar(10)
    cuenta.retirar(5)

    assert cuenta.saldo == 5


def test_no_puedo_retirar_mas_dinero_del_que_tengo():
    # -- mil quilombos que termina en esto xd
    cuenta = Cuenta()
    cuenta.depositar(10)
    # cuenta.retirar(50)
    try:
        cuenta.retirar(50)
        pytest.fail('No puede sacar 50 xd')
    except ValueError:
        assert True
