class Cuenta:
    def __init__(self):
        self.saldo = 0

    def depositar(self, dinero):
        self.saldo += dinero

    def retirar(self, dinero):
        if dinero > self.saldo:
            # Genero un error con RAISE
            raise ValueError('No tenes plata')
        self.saldo -= dinero


''' Excepciones

Representan situaciones EXCEPCIONALES

A nivel tecnico hay dos excepciones
- Errores técnicos son imprevistos.
    Por ejemplo un error porque no hay internet o la pc se quedo sin memoria, sin disco. En general la aplicacion ya no tiene nada por hacer. Por ejemplo la famosa pantalla azul de windows.
    O cuando vas al cajero automatico y queres sacar plata, y este no tiene plata. No hay más que hacer. Tambien puede ser que el cajero no se este conectando bien y no sabe cuanta plata tenes.
    "Volve más tarde" "Opppps!"
- Errores de negocio de reglas{ try raise except},
    Error del usuario, que quiere hacer algo que no esta previsto. Muy posiblemente de la opcion de reintentar
    Por ejemplo en mi cuenta del banco tengo 500. El chabon quiere sacar 1000 entonces el cajero niega y dice nooo mira, tenes 500, no podes sacar 1000"

A estos problemas no hay nada que solucionar.
Solo detecta el problema y da un aviso
'''
