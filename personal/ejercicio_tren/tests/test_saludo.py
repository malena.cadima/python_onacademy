from app.saludo import Vagon, Locomotora, Tren
import pytest


'''Un tren tiene una locomotora y una determinada cantidad de vagones en relación a la potencia de la locomotora.

Potencia hasta 1000 HP, hasta 4 vagones
Potencia hasta 2000 HP, hasta 7 vagones

La cantidad de pasajeros que puede transportar el tren está dada por la suma de la capacidad de sus vagones. Cada vagón tiene una capacidad para pasajeros sentados y una capacidad para pasajeros parados.

Se pide modelar este problema y agregar la lógica para determinar la total cantidad de pasajeros de un determinado tren.
'''

def test_capacidad_total_del_vagon():
    primer_vagon = Vagon(20, 10)

    assert primer_vagon.capacidad() == 30


def test_locomotora_hasta_1000hp_y_4_vagones():
    potencia = 1000
    locomotora = Locomotora(potencia)

    assert locomotora.potencia ==  1000


def test_construir_un_tren():
    potencia = 1000
    locomotora = Locomotora(potencia)

    assert Tren(locomotora)


def test_agregar_vagon():
    vagon = Vagon(10, 20)


    tren = Tren(1000)

    # Falta (?)
