class Vagon:
    def __init__(self, personas_paradas, personas_sentadas):
        self.personas_paradas = personas_paradas
        self.personas_sentadas = personas_sentadas

    def capacidad(self):
        return self.personas_paradas + self.personas_sentadas


class Locomotora:
    def __init__(self, potencia):
        self.potencia = potencia


class Tren:
    def __init__(self, locomotora):
        self.locomotora = locomotora
        self.lista_vagones = []

    def agregar_vagones(self, un_vagon):
        self.lista_vagones.append(un_vagon)
