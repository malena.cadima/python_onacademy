class Punto:
    ''' Representacion de un punto en el plano en coordenadas cartesianas (x,y) '''
    def __init__(self, x, y):
        'Constructor de Punto. x e y deben ser numericos'
        self.x = self.validar_numero(x)
        self.y = self.validar_numero(y)

    def validar_numero(self, valor):
        if not isinstance(valor, (int, float, complex)):
            raise ValueError('{:r} no es un valor numerico'.format(valor))
        return valor

    # Si queremos sumar dos puntos:
    def __add__(self, otro):
        return Punto(self.x + otro.x , self.y + otro.y)

    # Si queremos restar dos puntos 
    def __sub__(self, otro):
        return Punto(self.x - otro.x , self.y - otro.y)


