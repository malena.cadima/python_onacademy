class Hotel:
    ''' Representar un Hotel: sus atributos son: nombre, ubicacion, puntaje y precio '''
    
    def __init__(self, nombre, ubicacion, puntaje, precio):
        ''' Crea un Hotel.
            nombre y ubicacion deben ser cadenas no vacias,
            puntaje y precio son numericos. '''
        self.nombre = self.validar_cadena_no_vacia(nombre)
        self.ubicacion = self.validar_cadena_no_vacia(ubicacion)
        self.puntaje = self.validar_numero_positivo(puntaje)
        self.precio = self.validar_numero_positivo(precio)


    def __str__(self):
        ''' Conversion a cadena de texto '''
        return '{} de {} - Puntaje: {} - Precio: {} pesos'.format(
            self.nombre,
            self.ubicacion,
            self.puntaje,
            self.precio
        )


    def ratio(self):
        ''' Calcula la relacion calidad-precio de un hotel '''
        return ((self.puntaje ** 2) * 10 / self.precio)


    def validar_cadena_no_vacia(self, valor):
        if not isinstance(valor, str):
            raise TypeError('{} no es una cadena de texto'.format(valor))
        if len(valor) == 0:
            raise ValueError('{} no puede estar vacio.'.format(valor))
        return valor


    def validar_numero_positivo(self, valor):
        if not isinstance(valor, (int, float)):
            raise TypeError('{} no es un dato numerico'.format(valor))
        
        if valor < 0:
            raise ValueError('{} no es un numero positivo.'.format(valor))
        return valor
