from app.carta import Carta
from app.carta import Mano

#------------------------------------------ esto escribio el profe para explicar

def test_dos_cartas_son_iguales():
    una_carta = '1|e'
    otra_carta = '1|e'

    assert una_carta == otra_carta

def test_dos_cartas_mismo_palo():
    una_carta = '2|e'
    otra_carta = '1|e'
    assert una_carta[-1] == otra_carta[-1]

def test_dos_cartas_mismo_palo_mejorado():
    una_carta = Carta(2, 'espada')
    otra_carta = Carta(1, 'espada')

    assert una_carta.es_mismo_palo_que(otra_carta)

# def test_dos_cartas_son_iguales_2():
#     una_carta = Carta(1, 'espada')
#     otra_carta = Carta(1, 'espada')

#     assert una_carta.es_igual_a(otra_carta)

#------------------------------------------ esto pidio el profe para hacer ahora

def test_una_carta_es_mayor_a_otra_carta():
    una_carta = Carta(5, "espada")
    otra_carta = Carta(1, "espada")

    assert una_carta.es_mayor_a(otra_carta)


def test_una_carta_distinta_a_carta():
    una_carta = Carta(5, "espada")
    otra_carta = Carta(1, "espada")

    assert una_carta.es_distinta_a(otra_carta)

def test_tres_cartas_forman_flor():
    carta_1 = Carta(1, "espada")
    carta_2 = Carta(2, "espada")
    carta_3 = Carta(3, "espada")

    assert carta_1.forma_flor_con(carta_2, carta_3)

def test_dos_cartas_no_forman_flor():
    carta_1 = Carta(1, "espada")
    carta_2 = Carta(2, "oro")
    carta_3 = Carta(3, "espada")

    assert carta_1.no_forma_flor_con(carta_2, carta_3)

#--------------- segunda parte de la clase con Mano y Baraja

def test_baraja_no_repite_cartas():
    baraja = Baraja()
    assert len(baraja.cartas) == 40

    assert baraja.obtener_carta(12, 'oro') != None
    assert len(baraja.cartas) == 39

    assert baraja.obtener_carta(13, 'oro') == None
    assert len(baraja.cartas) == 39


def test_calcular_envido_con_3_cartas():
    carta_1 = Carta(1, 'espada')
    carta_2 = Carta(1, 'espada')
    carta_3 = Carta(3, 'espada')

