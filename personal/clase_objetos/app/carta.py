class Baraja:
    def __init__(self):
        self.cartas = []
        palos = ['oro', 'espada', 'copa', 'basto']
        cantidad_cartas = 50
        indice = 1
        while indice < cantidad_cartas:
            for palo in palos:
                self.cartas.append(Carta(indice, palo))
            indice += 1

    def obtener_carta(self, numero, palo):
        # return Carta(numero, palo)
        for carta in self.cartas:
            if carta.numero == numero and carta.palo == palo:
                self.cartas.remove(carta)
                return carta

class Carta:
    def __init__(self, numero, palo):
        self.numero = numero
        self.palo = palo

    def es_mismo_palo_que(self, otra_carta):
        return self.palo == otra_carta.palo

    def es_igual_a(self, otra_carta):
        return self.palo == otra_carta.palo and self.numero == otra_carta.numero

#---------------- Hasta acá el profe escribio para explicar, lo siguiente es el ejercicio resuelto

    def es_mayor_a(self, otra_carta):
        return self.numero > otra_carta.numero

    def es_distinta_a(self, otra_carta):
        return self.palo != otra_carta.palo or self.numero != otra_carta.numero

    def forma_flor_con(self, carta_2, carta_3):
        return self.palo == carta_2.palo == carta_3.palo

    def no_forma_flor_con(self, carta_2, carta_3):
        return not self.forma_flor_con(carta_2, carta_3)

class Mano:
    def __init__(self, carta_1, carta_2, carta_3):
        self.carta_1 = carta_1
        self.carta_2 = carta_2
        self.carta_3 = carta_3

    def calcular_envido(self):
        envido_1 = self.carta_1.calcular_envido_con(carta_2)
        envido_2 = self.carta_1.calcular_envido_con(carta_3)
        envido_3 = self.carta_2.calcular_envido_con(carta_3)
        return max (envido_1, envido_2, envido_3)
