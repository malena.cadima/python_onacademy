class Persona:
    def hablar(self):
        return "bla"

    def cantidad_de_patas(self):
        return 2


class Perro:
    def ladrar(self):
        return "guau"

    def cantidad_de_patas(self):
        return 4


class Paloma:
    def volar(self):
        return "mueve las alas"

    def cantidad_de_patas(self):
        return 2

class Contador:
    @staticmethod
    def contar_patas_de(lista_de_animales):
        total_de_patas = 0
        for animal in lista_de_animales:
            total_de_patas += animal.cantidad_de_patas()

        return total_de_patas
